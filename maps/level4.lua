return {
  tilewidth = 16,
  tileheight = 16,
  layerImages = {"maps/level4/tiles_01", "maps/level4/tiles_02", "maps/level4/tiles_03"},
  objectImage = "maps/level4/objects",
  triggerImage = "maps/level4/triggers",
  doorImage = "maps/level4/doors",
  dudeImage = "maps/level4/dudes",
  exitImage = "maps/level4/exits",
  tilesheets = {"tilesheets/tilesheet", "tilesheets/blocks", "tilesheets/triggers", "tilesheets/doors", "tilesheets/locks"}
}
return {
  tilewidth = 16,
  tileheight = 16,
  layerImages = {"maps/level3/tiles_01", "maps/level3/tiles_02"},
  objectImage = "maps/level3/objects",
  triggerImage = "maps/level3/triggers",
  doorImage = "maps/level3/doors",
  dudeImage = "maps/level3/dudes",
  exitImage = "maps/level3/exits",
  tilesheets = {"tilesheets/tilesheet", "tilesheets/blocks", "tilesheets/triggers", "tilesheets/doors", "tilesheets/locks"}
}
return {
  tilewidth = 16,
  tileheight = 16,
  layerImages = {"maps/level2/tiles_01", "maps/level2/tiles_02", "maps/level2/tiles_03"},
  objectImage = "maps/level2/objects",
  triggerImage = "maps/level2/triggers",
  doorImage = "maps/level2/doors",
  dudeImage = "maps/level2/dudes",
  exitImage = "maps/level2/exits",
  tilesheets = {"tilesheets/tilesheet", "tilesheets/blocks", "tilesheets/triggers", "tilesheets/doors", "tilesheets/locks"}
}
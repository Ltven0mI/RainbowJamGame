return {
  tilewidth = 16,
  tileheight = 16,
  layerImages = {"maps/level1/tiles_01", "maps/level1/tiles_02", "maps/level1/tiles_03"},
  objectImage = "maps/level1/objects",
  triggerImage = "maps/level1/triggers",
  doorImage = "maps/level1/doors",
  dudeImage = "maps/level1/dudes",
  exitImage = "maps/level1/exits",
  tilesheets = {"tilesheets/tilesheet", "tilesheets/blocks", "tilesheets/triggers", "tilesheets/doors", "tilesheets/locks"}
}
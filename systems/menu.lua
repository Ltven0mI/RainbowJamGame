local public = {__label="menu", __states="menu", __order={load=1, logic=1, draw=1}}

public.controlImage = nil
public.titleImage = nil
public.textImages = {}

public.rotSpeed = 140
public.rotInc = 25
public.rot = 0
public.waveMultiplier = 2

public.bgm = nil

public.countDownTime = 0
public.controlPauseTime = 7
public.fadePauseTime = 1.5
public.fadeTime = 0.8

public.isFading = false
public.fadeColour = {11, 9, 13, 255}
public.fade = 1
public.fadeState = 1

function public.load()
  public.controlImage = core.getAsset("controls")
  public.titleImage = core.getAsset("title")

  table.insert(public.textImages, core.getAsset("text_01"))
  table.insert(public.textImages, core.getAsset("text_02"))
  table.insert(public.textImages, core.getAsset("text_03"))
  table.insert(public.textImages, core.getAsset("text_04"))
  table.insert(public.textImages, core.getAsset("text_05"))
  table.insert(public.textImages, core.getAsset("text_06"))
  table.insert(public.textImages, core.getAsset("text_07"))
  table.insert(public.textImages, core.getAsset("text_08"))
  table.insert(public.textImages, core.getAsset("text_09"))
  table.insert(public.textImages, core.getAsset("text_10"))
  table.insert(public.textImages, core.getAsset("text_11"))

  public.bgm = love.audio.newSource("assets/sounds/music/metabreak.wav", "static")
  public.bgm:play()
  public.bgm:setLooping(true)
  public.bgm:setVolume(0.5)
end

function public.update(dt)
  public.rot = public.rot + public.rotSpeed * dt

  if public.isFading then
    if public.fadeState == 1 then
      public.fade = public.fade - (1/public.fadeTime) * dt
    elseif public.fadeState == 2 then
      if public.countDownTime > 0 then
        public.countDownTime = public.countDownTime - dt
      else
        public.countDownTime = 0
        public.fade = public.fade + (1/public.fadeTime) * dt
      end
    elseif public.fadeState == 3 then
      if public.countDownTime > 0 then
        public.countDownTime = public.countDownTime - dt
      else
        public.countDownTime = 0
        public.fade = public.fade - (1/public.fadeTime) * dt
      end
    elseif public.fadeState == 4 then
      if public.countDownTime > 0 then
        public.countDownTime = public.countDownTime - dt
      else
        public.countDownTime = 0
        public.isFading = false
        core.setState("game")
      end
    elseif public.fadeState == 5 then
      public.fade = public.fade + (1/public.fadeTime) * dt
    end

    if public.fade <= 0 then
      public.fade = 0
      if public.fadeState == 1 then
        public.fadeState = 2
        public.countDownTime = public.fadePauseTime
      elseif public.fadeState == 3 then
        public.fadeState = 4
        public.countDownTime = public.fadePauseTime
      end
    end

    if public.fade >= 1 then
      public.fade = 1
      if public.fadeState == 2 then
        public.fadeState = 3
        public.countDownTime = public.controlPauseTime
      elseif public.fadeState == 5 then
        public.isFading = false
        public.fade = 1
        public.fadeState = 1
        public.countDownTime = 0
      end
    end
    if public.fadeState == 1 or public.fadeState == 5 then
      public.bgm:setVolume(public.fade)
    else
      public.bgm:setVolume(0)
    end
  end
end

function public.draw()
  local width, height = core.getDimensions()

  love.graphics.setColor(public.fadeColour)
  love.graphics.rectangle("fill", 0, 0, width, height)

  love.graphics.setColor(255, 255, 255, 255 * public.fade)

  if public.fadeState == 1 or public.fadeState == 5 then
    love.graphics.draw(public.titleImage, 0, 0)

    local halfW = math.floor(width / 2)

    for i, image in ipairs(public.textImages) do
      local yOffset = -math.cos(math.rad(public.rot - public.rotInc * (i-1))) * public.waveMultiplier
      love.graphics.draw(image, halfW - math.floor(image:getWidth() / 2), height - 20 + yOffset)
    end
  else
    love.graphics.draw(public.controlImage, 0, 0)
  end
end

function public.keypressed(key)
  if key ~= "escape" then
    if public.isFading == false then
      public.doFade()
    else
      public.countDownTime = 0
    end
  else
    love.event.quit()
  end
end

function public.onEnabled(doFadeIn)
  if public.bgm ~= nil then
    public.bgm:play()
    public.bgm:setLooping(true)
  end

  public.rot = 0

  public.countDownTime = 0

  public.isFading = false
  public.fade = 1
  public.fadeState = 1

  if doFadeIn then
    public.isFading = true
    public.fade = 0
    public.fadeState = 5
  end
end

function public.onDisabled()
  public.bgm:stop()
end

function public.doFade()
  if not public.isFading then
    public.isFading = true
    public.fade = 1
    public.fadeState = 1
  end
end

return public
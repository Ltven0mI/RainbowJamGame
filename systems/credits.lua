local public = {__label="credits", __states="credits", __order={load=1, logic=1, draw=1}}

public.creditImage = nil

public.countDownTime = 0
public.creditsPauseTime = 9
public.fadePauseTime = 1.5
public.fadeTime = 0.8

public.isFading = true
public.fadeColour = {11, 9, 13, 255}
public.fade = 0
public.fadeState = 0
public.fadeEnd = nil

public.dudeImages = {}
public.dudeQuads = {}
public.dudes = {}

public.dudeMoveSpeed = 10
public.dudeAnimSpeed = 6


function public.load()
  public.fadeEnd = public.backToMenu
  public.creditImage = core.getAsset("creditsImage")

  table.insert(public.dudeImages, core.getAsset("dudes/red/right"))
  table.insert(public.dudeImages, core.getAsset("dudes/orange/right"))
  table.insert(public.dudeImages, core.getAsset("dudes/yellow/right"))
  table.insert(public.dudeImages, core.getAsset("dudes/green/right"))
  table.insert(public.dudeImages, core.getAsset("dudes/blue/right"))
  table.insert(public.dudeImages, core.getAsset("dudes/indigo/right"))
  table.insert(public.dudeImages, core.getAsset("dudes/purple/right"))

  public.dudeQuads[1] = love.graphics.newQuad(0, 0, 16, 16, 32, 16)
  public.dudeQuads[2] = love.graphics.newQuad(16, 0, 16, 16, 32, 16)
end

function public.update(dt)
  if public.isFading then
    if public.fadeState == 0 then
      public.fade = public.fade + (1/public.fadeTime) * dt
    elseif public.fadeState == 1 then
      if public.countDownTime > 0 then
        public.countDownTime = public.countDownTime - dt
      else
        public.countDownTime = 0
        public.fade = public.fade - (1/public.fadeTime) * dt
      end
    elseif public.fadeState == 2 then
      if public.countDownTime > 0 then
        public.countDownTime = public.countDownTime - dt
      else
        public.countDownTime = 0
        public.isFading = false
        public.fadeEnd()
      end
    end

    if public.fade <= 0 then
      public.fade = 0
      if public.fadeState == 1 then
        public.fadeState = 2
        public.countDownTime = public.fadePauseTime
      end
    end

    if public.fade >= 1 then
      public.fade = 1
      if public.fadeState == 0 then
        public.fadeState = 1
        public.countDownTime = public.creditsPauseTime
      end
    end
  end
  for k, dude in pairs(public.dudes) do
    dude.x = dude.x + public.dudeMoveSpeed * dt
    dude.animStep = dude.animStep + public.dudeAnimSpeed * dt
    if dude.animStep >= 2 then
      dude.animStep = dude.animStep % 2
    end
  end
end

function public.draw()
  local width, height = core.getDimensions()

  love.graphics.setColor(public.fadeColour)
  love.graphics.rectangle("fill", 0, 0, width, height)

  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.draw(public.creditImage, 0, 0)

  for k, dude in pairs(public.dudes) do
    love.graphics.draw(public.dudeImages[dude.id], public.dudeQuads[math.floor(dude.animStep) + 1], dude.x, height - 34)
  end

  love.graphics.setColor(public.fadeColour[1], public.fadeColour[2], public.fadeColour[3], 255 * (-public.fade+1))
  love.graphics.rectangle("fill", 0, 0, width, height)
end

function public.onEnabled()
  public.countDownTime = 0

  public.isFading = true
  public.fade = 0
  public.fadeState = 0

  for i=1, 7 do
    public.addDude((-i)+8, (i-1) * 18 + 2)
  end
end

function public.onDisabled()
  public.dudes = {}
end

function public.backToMenu()
  core.setState("menu", true)
end

function public.keypressed(key)
  public.countDownTime = 0
end

function public.addDude(id, x)
  local x = x or 0
  table.insert(public.dudes, {x=x, animStep=math.random()*2, id=id})
end

return public
local public = {__label="world", __states="game", __order={load=1, logic=1, draw=1}}

public.tilesheet = nil
public.blockTilesheet = nil
public.triggerTilesheet = nil

public.levelID = 1
public.levels = {}
public.currentLevel = nil

public.camera = {x=0, y=0}

public.dudes = {}
public.currentDude = nil


public.bgm = nil


public.countDownTime = 0
public.controlPauseTime = 7
public.endPauseTime = 1.5
public.fadePauseTime = 0.8
public.fadeTime = 0.6

public.isFading = true
public.fadeColour = {11, 9, 13, 255}
public.fade = 1
public.fadeState = 0
public.fadeEnd = nil

public.fadeShader = nil
public.fadeBG = nil

function public.load()
  local msg

  public.tilesheet, msg = tilesheet.new(core.getAsset("tilesheets/tilesheet"), 16, 16)
  if public.tilesheet == nil then
    error(msg)
  end

  public.blockTilesheet, msg = tilesheet.new(core.getAsset("tilesheets/blocks"), 16, 16)
  if public.blockTilesheet == nil then
    error(msg)
  end

  public.triggerTilesheet, msg = tilesheet.new(core.getAsset("tilesheets/triggers"), 16, 16)
  if public.triggerTilesheet == nil then
    error(msg)
  end

  public.doorTilesheet, msg = tilesheet.new(core.getAsset("tilesheets/doors"), 16, 16)
  if public.doorTilesheet == nil then
    error(msg)
  end

  public.lockTilesheet, msg = tilesheet.new(core.getAsset("tilesheets/locks"), 16, 16)
  if public.lockTilesheet == nil then
    error(msg)
  end

  local tilesheets = {
    public.tilesheet,
    public.blockTilesheet,
    public.triggerTilesheet,
    public.doorTilesheet,
    public.lockTilesheet
  }


  public.buttonClickSound = love.audio.newSource("assets/sounds/buttonclick.wav", "static")
  public.buttonClickSound:setVolume(0.9)

  public.buttonFailedSound = love.audio.newSource("assets/sounds/buttonfailed.wav", "static")
  public.buttonFailedSound:setVolume(0.5)


  local redImages = {
    upleft=core.getAsset("dudes/red/upleft"),
    upright=core.getAsset("dudes/red/upright"),
    downleft=core.getAsset("dudes/red/left"),
    downright=core.getAsset("dudes/red/right"),
  }
  table.insert(public.dudes, redImages)

  local orangeImages = {
    upleft=core.getAsset("dudes/orange/upleft"),
    upright=core.getAsset("dudes/orange/upright"),
    downleft=core.getAsset("dudes/orange/left"),
    downright=core.getAsset("dudes/orange/right"),
  }
  table.insert(public.dudes, orangeImages)

  local yellowImages = {
    upleft=core.getAsset("dudes/yellow/upleft"),
    upright=core.getAsset("dudes/yellow/upright"),
    downleft=core.getAsset("dudes/yellow/left"),
    downright=core.getAsset("dudes/yellow/right"),
  }
  table.insert(public.dudes, yellowImages)

  local greenImages = {
    upleft=core.getAsset("dudes/green/upleft"),
    upright=core.getAsset("dudes/green/upright"),
    downleft=core.getAsset("dudes/green/left"),
    downright=core.getAsset("dudes/green/right"),
  }
  table.insert(public.dudes, greenImages)

  local blueImages = {
    upleft=core.getAsset("dudes/blue/upleft"),
    upright=core.getAsset("dudes/blue/upright"),
    downleft=core.getAsset("dudes/blue/left"),
    downright=core.getAsset("dudes/blue/right"),
  }
  table.insert(public.dudes, blueImages)

  local indigoImages = {
    upleft=core.getAsset("dudes/indigo/upleft"),
    upright=core.getAsset("dudes/indigo/upright"),
    downleft=core.getAsset("dudes/indigo/left"),
    downright=core.getAsset("dudes/indigo/right"),
  }
  table.insert(public.dudes, indigoImages)

  local purpleImages = {
    upleft=core.getAsset("dudes/purple/upleft"),
    upright=core.getAsset("dudes/purple/upright"),
    downleft=core.getAsset("dudes/purple/left"),
    downright=core.getAsset("dudes/purple/right"),
  }
  table.insert(public.dudes, purpleImages)

  local level1_data = require("maps.level1")
  table.insert(public.levels, level1_data)
  local level2_data = require("maps.level2")
  table.insert(public.levels, level2_data)
  local level3_data = require("maps.level3")
  table.insert(public.levels, level3_data)
  local level4_data = require("maps.level4")
  table.insert(public.levels, level4_data)

  public.bgm = love.audio.newSource("assets/sounds/music/sudo.wav", "static")
  public.bgm:setLooping(true)
  public.bgm:setVolume(0)

  public.fadeShader = love.graphics.newShader[[
    extern Image fadeImage;
    extern number cutoff;
    vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ){
      vec4 cutoffPixel = Texel(fadeImage, texture_coords);

      if (cutoffPixel.r > cutoff)
      {
        return color*cutoff;
      }

      return Texel(texture, texture_coords) * color;
    }
  ]]
  public.fadeShader:send("fadeImage", core.getAsset("fadeImage"))
  public.fadeShader:send("cutoff", 1)

  public.fadeBG = core.getAsset("fadeBG")
end

function public.update(dt)
  if public.isFading then
    if public.fadeState == 0 then
      public.fade = public.fade - (1/public.fadeTime) * dt
    elseif public.fadeState == 1 then
      public.fade = public.fade + (1/public.fadeTime) * dt
    elseif public.fadeState == 2 then
      if public.countDownTime > 0 then
        public.countDownTime = public.countDownTime - dt
      else
        public.countDownTime = 0
        public.fade = public.fade - (1/public.fadeTime) * dt
      end
    elseif public.fadeState == 3 then
      public.fade = public.fade + (1/public.fadeTime) * dt
    elseif public.fadeState == 4 then
      if public.countDownTime > 0 then
        public.countDownTime = public.countDownTime - dt
      else
        public.countDownTime = 0
        public.isFading = false
        public.fadeState = 1
        public.fadeEnd()
      end
    end

    if public.fade >= 1 then
      public.fade = 1
      if public.fadeState == 1 then
        public.fadeState = 2
        public.countDownTime = public.fadePauseTime
        public.fadeEnd()
      elseif public.fadeState == 3 then
        public.fadeState = 4
        public.countDownTime = public.endPauseTime
      end
    end

    if public.fade <= 0 then
      public.fade = 0
      if public.fadeState == 0 then
        public.isFading = false
      elseif public.fadeState == 2 then
        public.isFading = false
      end
    end

    public.bgm:setVolume(-public.fade+1)
  else
    local up, down, left, right = love.keyboard.isDown("w"), love.keyboard.isDown("s"), love.keyboard.isDown("a"), love.keyboard.isDown("d")
    if up and not down then
      public.currentDude:moveUp()
    end
    if down and not up then
      public.currentDude:moveDown()
    end
    if left and not right  then
      public.currentDude:moveLeft()
    end
    if right and not left then
      public.currentDude:moveRight()
    end

    public.currentLevel:update(dt)
  end

  local curLevel = public.currentLevel
  local width, height = core.getDimensions()
  local halfW, halfH = math.floor(width / 2), math.floor(height / 2)
  local tileFitX = math.min(math.ceil(width / curLevel.tilewidth), curLevel.width)
  local tileFitY = math.min(math.ceil(height / curLevel.tileheight), curLevel.height)
  local player = public.currentDude
  local playerX, playerY = math.floor((0.5 + player.offsetX + player.x - 1) * curLevel.tilewidth), math.floor((0.5 + player.offsetY + player.y - 1) * curLevel.tileheight)

  local newCamX, newCamY = playerX - halfW, playerY - halfH
  if newCamX < 0 then newCamX = 0 end
  if newCamX > (curLevel.width - tileFitX) * curLevel.tilewidth then newCamX = (curLevel.width - tileFitX) * curLevel.tilewidth end
  if newCamY < 0 then newCamY = 0 end
  if newCamY > (curLevel.height - tileFitY) * curLevel.tileheight then newCamY = (curLevel.height - tileFitY) * curLevel.tileheight end
  public.camera.x = newCamX
  public.camera.y = newCamY
end

function public.draw()
  local width, height = core.getDimensions()

	love.graphics.setColor(255, 255, 255, 255)
  public.currentLevel:draw(-math.floor(public.camera.x), -math.floor(public.camera.y), 1, 1)

  if public.isFading then
    public.fadeColour[4] = 255-- * public.fade
    public.fadeShader:send("cutoff", public.fade)
    love.graphics.setColor(public.fadeColour)
    love.graphics.setShader(public.fadeShader)
    -- love.graphics.rectangle("fill", 0, 0, width, height)
    love.graphics.draw(public.fadeBG, 0, 0)
    love.graphics.setShader()
  end

  -- love.graphics.setColor(255, 255, 255, 255)
  -- love.graphics.print(tostring(love.timer.getFPS()), 0, 0)
end

function public.keypressed(key)
  if not public.isFading then
    local dudeNum = tonumber(key)
    if dudeNum ~= nil and dudeNum > 0 and public.currentLevel.dudeLayer.list[dudeNum] ~= nil then
      public.currentDude = public.currentLevel.dudeLayer.list[dudeNum]
      public.currentLevel.dudeLayer.activeID = dudeNum
    end
    if key == "r" then
      public.fadeReload()
    end
    -- if key == "right" then
    --   public.fadeToNext()
    -- end
    -- if key == "delete" then
    --   print("screenshot")
    --   public.currentLevel:takeScreenshot()
    -- end
  end
end


function public.onEnabled()
  public.levelID = 1
  local success, msg = public.setLevel(public.levelID)
  if not success then
    error(msg)
  end

  public.countDownTime = 0

  public.isFading = true
  public.fade = 1
  public.fadeState = 0

  public.bgm:play()
  public.bgm:setLooping(true)
  public.bgm:setVolume(-public.fade+1)
end

function public.onDisabled()
  public.bgm:stop()
end


function public.fadeToEnd()
  public.fade = 0
  public.isFading = true
  public.fadeState = 3
  public.fadeEnd = public.onFinished
end

function public.onFinished()
  core.setState("credits")
end

function public.fadeReload()
  public.fade = 0
  public.isFading = true
  public.fadeState = 1
  public.fadeEnd = public.reloadLevel
end

function public.reloadLevel()
  local success, msg = public.setLevel(public.levelID)
  if not success then
    return false, msg
  end
  return true
end

function public.nextLevel()
  local success, msg = public.setLevel(public.levelID + 1)
  if not success then
    return nil, msg
  end
  public.levelID = public.levelID + 1
  return true
end

function public.fadeToNext()
  if public.levelID < #public.levels then
    public.fade = 0
    public.isFading = true
    public.fadeState = 1
    public.fadeEnd = public.nextLevel
  else
    public.fadeToEnd()
  end
end

function public.preLevel()
  local success, msg = public.setLevel(public.levelID - 1)
  if not success then
    return false, msg
  end
  public.levelID = public.levelID - 1
  return true
end

function public.setLevel(id)
  if id < 1 or id > #public.levels then
    return false, "[World] SetLevel id must be between 1 and "..#public.levels
  end
  local loadedLevel, msg = public.loadLevel(public.levels[id])
  if loadedLevel == nil then
    return false, msg
  end
  loadedLevel.nextLevel = public.fadeToNext
  public.currentLevel = loadedLevel
  public.currentDude = loadedLevel.dudeLayer.list[1]
  return true
end


function public.loadLevel(levelData)
  local tilewidth = levelData.tilewidth
  local tileheight = levelData.tileheight
  if tilewidth == nil or tileheight == nil then
    return nil, "[World] LevelData tilewidth and tileheight can't be nil!"
  end

  -- Tilesheets --
  if type(levelData.tilesheets) ~= "table" then
    return nil, "[World] LevelData tilesheets must be of type table! Not "..type(levelData.tilesheets)
  end

  local tilesheets = {}
  for k, tilesheetName in ipairs(levelData.tilesheets) do
    local tilesheet, msg = tilesheet.new(core.getAsset(tilesheetName), tilewidth, tileheight)
    if tilesheet == nil then
      return nil, msg
    end
    table.insert(tilesheets, tilesheet)
  end

  -- Tile Layers --
  if type(levelData.layerImages) ~= "table" then
    return nil, "[World] LevelData layerImages must be of type table! Not "..type(levelData.layerImages)
  end

  local layers = {}
  for k, layerName in ipairs(levelData.layerImages) do
    if type(layerName) ~= "string" then
      return nil, "[World] LevelData layerImages must only contain strings! Not "..type(layerName)
    end

    local layerImage = core.getAsset(layerName)
    if layerImage == nil then
      return nil, "[World] No image with key: "..layerName
    end
    table.insert(layers, layerImage)
  end

  -- Object Layer --
  if type(levelData.objectImage) ~= "string" then
    return nil, "[World] LevelData objectImage must be of type string! Not "..type(levelData.objectImage)
  end

  local objectImage = core.getAsset(levelData.objectImage)
  if objectImage == nil then
    return nil, "[World] No image with key: "..levelData.objectImage
  end

  -- Trigger Layer --
  if type(levelData.triggerImage) ~= "string" then
    return nil, "[World] LevelData triggerImage must be of type string! Not "..type(levelData.triggerImage)
  end

  local triggerImage = core.getAsset(levelData.triggerImage)
  if triggerImage == nil then
    return nil, "[World] No image with key: "..levelData.triggerImage
  end

  -- Door Layer --
  if type(levelData.doorImage) ~= "string" then
    return nil, "[World] LevelData doorImage must be of type string! Not "..type(levelData.doorImage)
  end

  local doorImage = core.getAsset(levelData.doorImage)
  if doorImage == nil then
    return nil, "[World] No image with key: "..levelData.doorImage
  end

  -- Dude Layer --
  if type(levelData.dudeImage) ~= "string" then
    return nil, "[World] LevelData dudeImage must be of type string! Not "..type(levelData.dudeImage)
  end

  local dudeImage = core.getAsset(levelData.dudeImage)
  if dudeImage == nil then
    return nil, "[World] No image with key: "..levelData.dudeImage
  end

  -- Exit Layer --
  if type(levelData.exitImage) ~= "string" then
    return nil, "[World] LevelData exitImage must be of type string! Not "..type(levelData.exitImage)
  end

  local exitImage = core.getAsset(levelData.exitImage)
  if exitImage == nil then
    return nil, "[World] No image with key: "..levelData.exitImage
  end

  -- Creating Level --
  local newLevel, msg = level.loadFromImage(layers, objectImage, triggerImage, doorImage, dudeImage, exitImage, tilesheets, public.dudes, public.buttonClickSound, public.buttonFailedSound)
  if newLevel == nil then
    return nil, msg
  end
  return newLevel
end

return public
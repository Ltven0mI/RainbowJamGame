--[[
  The MIT License (MIT)

  Copyright (c) 2016 Wade Xavier Rauschenbach

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
]]

return function(entryState, graphicsScale)
  local public = {}
  local private = {}
  
  public.currentState = entryState or "entry"
  
  private.graphicsScale = graphicsScale or 1
  private.priorityMax   = 50
  private.directories   = {
    systems="systems/",
    assets="assets/",
    objects="objects/"
  }
  
  private.callbacks = {
    "directorydropped", "draw", "filedropped", "focus", "gamepadaxis",
    "gamepadpressed", "gamepadreleased", "joystickadded", "joystickaxis",
    "joystickhat", "joystickpressed", "joystickreleased", "joystickremoved",
    "keypressed", "keyreleased", "load", "lowmemory", "mousefocus",
    "mousemoved", "mousepressed", "mousereleased", "quit", "resize",
    "textedited", "textinput", "threaderror", "touchmoved", "touchpressed",
    "touchreleased", "update", "visible", "wheelmoved"
  }
  
  
  -- Debug --
  private.debugEnabled = false

  function private.debugWrite(...)
    if private.debugEnabled then
      io.write(...)
    end
  end

  function private.debugPrint(...)
    if private.debugEnabled then
      print(...)
    end
  end

  local function log(...)
    print("[LOG] ", ...)
  end
  
  local function warn(...)
    print("[WARN] ", ...)
  end
  
  local function err(...)
    print("[ERR] ", ...)
  end


  -- Util --
  function public.getMousePosition()
    local mx, my = love.mouse.getPosition()
    return math.floor(mx / private.graphicsScale), math.floor(my / private.graphicsScale)
  end

  function public.getDimensions()
    return private.canvas:getDimensions()
  end
  
  
  -- States --
  public.states = {}
  private.states = {}
  
  
  function public.setState(state, ...)
    public.currentState = state

    for label, system in pairs(private.systems.enabled) do
      public.systems.disable(label)
    end

    if state == "*" then
      for label, system in pairs(private.systems.table) do
        public.systems.enable(label, ...)
      end
    else
      for label, system in pairs(private.systems.table) do
        if system.__states == "*" or system.__states == state then
          public.systems.enable(label, ...)
        elseif type(system.__states) == "table" then
          for k, state in pairs(system.__states) do
            if state == state then
              public.systems.enable(label, ...)
              break
            end
          end
        end
      end
    end
  end
  
  
  -- Systems --
  public.systems = {}
  private.systems = {}
  private.systems.table = {}
  private.systems.enabled = {}

  function public.getSystem(label)
    return private.systems.table[label]
  end
  
  function public.systems.enable(label, ...)
    local system = private.systems.table[label]
    if not system.__active and type(system.onEnabled) == "function" then system.onEnabled(...) end
    system.__active = true
    private.systems.enabled[label] = system
  end
  
  function public.systems.disable(label, ...)
    local system = private.systems.table[label]
    if system.__active and type(system.onDisabled) == "function" then system.onDisabled(...) end
    system.__active = false
    private.systems.enabled[label] = nil
  end
  
  function private.registerSystem(system, filename)
    private.debugWrite("Registering "..filename.." : ")
    if type(system.__label) == "string" then
      if private.systems.table[system.__label] == nil then
        private.systems.table[system.__label] = system
        system.__active = false
        private.debugWrite("Registered as "..system.__label..".\n")
      else
        private.debugWrite("Failed, label already in use.\n")
      end
    else
      private.debugWrite("Failed, missing label.\n")
    end
  end


  -- Assets --
  public.assets = {}
  private.assets = {}
  private.assets.table = {}

  function public.getAsset(assetName)
    return private.assets.table[assetName]
  end

  local function loadAssetsInDirectory(directory, keyPrefix, prefix)
    local keyPrefix = keyPrefix or ""
    local prefix = prefix or " |"
    local items = love.filesystem.getDirectoryItems(directory)
    for k, v in pairs(items) do
      local path = directory..v
      private.debugWrite(prefix.." Loading: "..v.."... ")
      if love.filesystem.isFile(path) then
        if v:sub(-4) == ".png" then
          local loadedImage = love.graphics.newImage(path)
          if loadedImage ~= nil then
            local assetKey = v:sub(0, -5)
            private.assets.table[keyPrefix..assetKey] = loadedImage
            private.debugWrite("Loaded as: "..keyPrefix..assetKey.."\n")
          else
            private.debugWrite("Failed! Could not load as Image!\n")
          end
        else
          private.debugWrite("Failed! Not an Image!\n")
        end
      elseif love.filesystem.isDirectory(path) then
        private.debugWrite("Is directory!\n")
        loadAssetsInDirectory(path.."/", keyPrefix..v.."/", prefix.." |")
        private.debugWrite(prefix.." Finished loading from: "..path.."\n")
      else
        private.debugWrite("Failed! Not a file or directory!\n")
      end
    end
  end

  function private.assets.loadAssets()
    private.debugPrint("Loading assets!")
    if love.filesystem.isDirectory(private.directories.assets) then
      loadAssetsInDirectory(private.directories.assets)
      private.debugPrint("Finished loading assets!")
    else
      private.debugPrint("Failed to load assets! No such directory: "..private.directories.assets)
    end
  end

  do
    private.assets.loadAssets()
  end


  -- Objects --
  public.objects = {}
  private.objects = {}
  private.objects.table = {}


  local function loadObjectsInDirectory(directory, keyPrefix, prefix)
    local keyPrefix = keyPrefix or ""
    local prefix = prefix or " |"
    local items = love.filesystem.getDirectoryItems(directory)
    for k, v in pairs(items) do
      local path = directory..v
      private.debugWrite(prefix.." Loading: "..v.."... ")
      if love.filesystem.isFile(path) then
        if v:sub(-4) == ".lua" then
          local loadedObject = require(path)
          if loadedObject ~= nil then
            local objectKey = v:sub(0, -5)
            private.assets.table[keyPrefix..objectKey] = loadedObject
            private.debugWrite("Loaded as: "..keyPrefix..objectKey.."\n")
          else
            private.debugWrite("Failed! Could not require object!\n")
          end
        else
          private.debugWrite("Failed! Not a lua file!\n")
        end
      elseif love.filesystem.isDirectory(path) then
        private.debugWrite("Is directory!\n")
        loadObjectsInDirectory(path.."/", keyPrefix..v.."/", prefix.." |")
        private.debugWrite(prefix.." Finished loading from: "..path.."\n")
      else
        private.debugWrite("Failed! Not a file or directory!\n")
      end
    end
  end

  function private.objects.loadObjects()
    private.debugPrint("Loading objects!")
    if love.filesystem.isDirectory(private.directories.objects) then
      loadObjectsInDirectory(private.directories.objects)
      private.debugPrint("Finished loading objects!")
    else
      private.debugPrint("Failed to load objects! No such directory: "..private.directories.objects)
    end
  end

  do
    private.objects.loadObjects()
  end


  -- Graphics Scaler --
  private.graphics = {}
  private.graphics.setCanvas = love.graphics.setCanvas

  function public.getGraphicsScale()
    return private.graphicsScale
  end

  function public.setGraphicsScale(graphicsScale)
    private.graphicsScale = graphicsScale
  end

  function private.updateCanvasSize()
    private.canvas = love.graphics.newCanvas(math.ceil(love.graphics.getWidth() / private.graphicsScale), math.ceil(love.graphics.getHeight() / private.graphicsScale))
    private.canvas:setFilter("nearest", "nearest")
  end

  function love.graphics.setCanvas(canvas, ...)
    if canvas == nil then
      return private.graphics.setCanvas(private.canvas)
    else
      return private.graphics.setCanvas(canvas, ...)
    end
  end


  do
    private.updateCanvasSize()
  end

  
  -- Callback Handlers--
  do
    for k, callback in pairs(private.callbacks) do
      if callback == "load" then
        love.load = function(...)
          local sortedSystems = {}
          local unsortedSystems = {}
          for label, system in pairs(private.systems.table) do
            if type(system.load) == "function" then
              if type(system.__priority) == "table" and system.__priority.load then
                sortedSystems[system.__priority.load] = system
              else
                table.insert(unsortedSystems, system)
              end
            end
          end
          for i=1, private.priorityMax do
            if sortedSystems[i] then
              sortedSystems[i].load(...)
            end
          end
          for i, system in pairs(unsortedSystems) do
            system.load(...)
          end
        end
      elseif callback == "draw" then
        love.draw = function(...)
          private.graphics.setCanvas(private.canvas)
          love.graphics.clear()

          local sortedSystems = {}
          local unsortedSystems = {}
          for label, system in pairs(private.systems.enabled) do
            if type(system.draw) == "function" then
              if type(system.__priority) == "table" and system.__priority.render then
                sortedSystems[system.__priority.render] = system
              else
                table.insert(unsortedSystems, system)
              end
            end
          end
          for i=1, private.priorityMax do
            if sortedSystems[i] then
              sortedSystems[i].draw(...)
            end
          end
          for i, system in pairs(unsortedSystems) do
            system.draw(...)
          end

          private.graphics.setCanvas()

          local r, g, b, a = love.graphics.getColor()
          love.graphics.setColor(255, 255, 255, 255)
          love.graphics.draw(private.canvas, 0, 0, 0, private.graphicsScale, private.graphicsScale)
          love.graphics.setColor(r, g, b, a)
        end
      elseif callback == "resize" then
        love["resize"] = function(w, h)
          private.updateCanvasSize()

          local sortedSystems = {}
          local unsortedSystems = {}
          for label, system in pairs(private.systems.enabled) do
            if type(system["resize"]) == "function" then
              if type(system.__priority) == "table" and system.__priority.logic then
                sortedSystems[system.__priority.logic] = system
              else
                table.insert(unsortedSystems, system)
              end
            end
          end
          for i=1, private.priorityMax do
            if sortedSystems[i] then
              sortedSystems[i]["resize"](math.ceil(w / private.graphicsScale), math.ceil(h / private.graphicsScale))
            end
          end
          for i, system in pairs(unsortedSystems) do
            system["resize"](math.ceil(w / private.graphicsScale), math.ceil(h / private.graphicsScale))
          end
        end
      else
        love[callback] = function(...)
          local sortedSystems = {}
          local unsortedSystems = {}
          for label, system in pairs(private.systems.enabled) do
            if type(system[callback]) == "function" then
              if type(system.__priority) == "table" and system.__priority.logic then
                sortedSystems[system.__priority.logic] = system
              else
                table.insert(unsortedSystems, system)
              end
            end
          end
          for i=1, private.priorityMax do
            if sortedSystems[i] then
              sortedSystems[i][callback](...)
            end
          end
          for i, system in pairs(unsortedSystems) do
            system[callback](...)
          end
        end
      end
    end
  end
  
  
  -- Register Systems --
  do
    if love.filesystem.isDirectory(private.directories.systems) then
      local possibleSystems = love.filesystem.getDirectoryItems(private.directories.systems)
      for k, filename in pairs(possibleSystems) do
        if filename:sub(-3) == "lua" then
          private.registerSystem(require(private.directories.systems.."/"..filename:sub(0, -5)), filename)
        end
      end
    else
      warn("System Directory "..private.directories.systems.." doesnt exist.")
    end
  end


  -- Enable Systems --
  do
    if public.currentState == "*" then
      for label, system in pairs(private.systems.table) do
        public.systems.enable(label)
      end
    else
      for label, system in pairs(private.systems.table) do
        if system.__states == "*" or system.__states == public.currentState then
          public.systems.enable(label)
        elseif type(system.__states) == "table" then
          for k, state in pairs(system.__states) do
            if state == public.currentState then
              public.systems.enable(label)
              break
            end
          end
        end
      end
    end
  end
  
  
  return public
end
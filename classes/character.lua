local public = {}
local private = {}
local meta = {}


-- MOVEDIR 1 == UP | 2 == DOWN | 3 == LEFT | 4 == RIGHT


-- Meta Data --
meta.__type = "character"

-- Meta Methods --
function meta.__index(t, key)
  return meta[key]
end

function meta.update(t, dt)
  if t.isMoving then
    t.movePercent = t.movePercent - t.speed * dt
    if t.movePercent < 0 then
      t.movePercent = 0
      t.isMoving = false
      t.level:onObjectFinishedMoving(t)
      local exitState = t.level:getExit(t.x, t.y)
      if exitState ~= nil then
        if exitState > 200 then
          t.level:nextLevel()
        elseif exitState <= 200 then
          t.level:preLevel()
        end
      end
    end

    if t.moveDir == 1 then
      if t.animHoriDir == 3 then
        t.currentSheet = t.spriteSheets.up.left
      elseif t.animHoriDir == 4 then
        t.currentSheet = t.spriteSheets.up.right
      end
    else
      if t.animHoriDir == 3 then
        t.currentSheet = t.spriteSheets.down.left
      elseif t.animHoriDir == 4 then
        t.currentSheet = t.spriteSheets.down.right
      end
    end

    t.animFrame = math.floor(t.movePercent * t.currentSheet.tileCount) + 1

    if t.moveDir == 1 then
      t.offsetY = t.movePercent
    elseif t.moveDir == 2 then
      t.offsetY = -t.movePercent
    elseif t.moveDir == 3 then
      t.offsetX = t.movePercent
    elseif t.moveDir == 4 then
      t.offsetX = -t.movePercent
    end
  end
end

function meta.draw(t, x, y, sx, sy)
  local x, y = x or 0, y or 0
  local tilewidth, tileheight = t.level.tilewidth * sx, t.level.tileheight * sy
  local drawX, drawY = x + math.floor((t.offsetX + t.x-1) * tilewidth), y + math.floor((t.offsetY + t.y-1) * tileheight)
  love.graphics.draw(t.currentSheet.image, t.currentSheet:getTile(t.animFrame), drawX, drawY, 0, sx, sy)
end

local function oppositeDir(dir)
  if dir == 1 then
    return 2
  elseif dir == 2 then
    return 1
  elseif dir == 3 then
    return 4
  elseif dir == 4 then
    return 3
  end
end

function meta.moveUp(t, force)
  local moveDir = 1
  if not t.isMoving then
    t.animVertDir = moveDir

    local nextX, nextY = t.x, t.y - 1
    if t.y > 1 and not t.level:getTileCollisionAt(nextX, nextY) and not t.level:getDoorCollisionAt(nextX, nextY) then
      local canMove = true
      local obj = t.level:getObjectAt(nextX, nextY)
      if obj ~= nil then
        canMove = t.level:pushObjectUp(nextX, nextY, t)
      end

      if canMove == true then
        local dudeInWay, msg = t.level:getDudeAt(nextX, nextY)
        if dudeInWay == nil then
          return nil, msg
        end
        if dudeInWay then
          if force and not dudeInWay.isMoving then
            return false
          else
            t.level:onObjectStartedMoving(t, nextX, nextY)
            t.isMoving = true
            t.moveDir = moveDir
            t.dir = t.moveDir
            t.movePercent = 1
            dudeInWay:moveDown(true)
            t.y = nextY
            return true
          end
        else
          t.level:onObjectStartedMoving(t, nextX, nextY)
          t.isMoving = true
          t.moveDir = moveDir
          t.dir = t.moveDir
          t.movePercent = 1
          t.y = nextY
          return true
        end
      end
    end
  end
  return false
end

function meta.moveDown(t, force)
  local moveDir = 2
  if not t.isMoving then
    t.animVertDir = moveDir

    local nextX, nextY = t.x, t.y + 1
    if t.y < t.level.height and not t.level:getTileCollisionAt(nextX, nextY) and not t.level:getDoorCollisionAt(nextX, nextY) then
      local canMove = true
      local obj = t.level:getObjectAt(nextX, nextY)
      if obj ~= nil then
        canMove = t.level:pushObjectDown(nextX, nextY, t)
      end

      if canMove == true then
        local dudeInWay, msg = t.level:getDudeAt(nextX, nextY)
        if dudeInWay == nil then
          return nil, msg
        end
        if dudeInWay then
          if force and not dudeInWay.isMoving then
            return false
          else
            t.level:onObjectStartedMoving(t, nextX, nextY)
            t.isMoving = true
            t.moveDir = moveDir
            t.dir = t.moveDir
            t.movePercent = 1
            dudeInWay:moveUp(true)
            t.y = nextY
            return true
          end
        else
          t.level:onObjectStartedMoving(t, nextX, nextY)
          t.isMoving = true
          t.moveDir = moveDir
          t.dir = t.moveDir
          t.movePercent = 1
          t.y = nextY
          return true
        end
      end
    end
  end
  return false
end

function meta.moveLeft(t, force)
  local moveDir = 3
  if not t.isMoving then
    t.animHoriDir = moveDir
    
    local nextX, nextY = t.x - 1, t.y
    if t.x > 1 and not t.level:getTileCollisionAt(nextX, nextY) and not t.level:getDoorCollisionAt(nextX, nextY) then
      local canMove = true
      local obj = t.level:getObjectAt(nextX, nextY)
      if obj ~= nil then
        canMove = t.level:pushObjectLeft(nextX, nextY, t)
      end

      if canMove == true then
        local dudeInWay, msg = t.level:getDudeAt(nextX, nextY)
        if dudeInWay == nil then
          return nil, msg
        end
        if dudeInWay then
          if force and not dudeInWay.isMoving then
            return false
          else
            t.level:onObjectStartedMoving(t, nextX, nextY)
            t.isMoving = true
            t.moveDir = moveDir
            t.dir = t.moveDir
            t.movePercent = 1
            dudeInWay:moveRight(true)
            t.x = nextX
            return true
          end
        else
          t.level:onObjectStartedMoving(t, nextX, nextY)
          t.isMoving = true
          t.moveDir = moveDir
          t.dir = t.moveDir
          t.movePercent = 1
          t.x = nextX
          return true
        end
      end
    end
  end
  return false
end

function meta.moveRight(t, force)
  local moveDir = 4
  if not t.isMoving then
    t.animHoriDir = moveDir
    
    local nextX, nextY = t.x + 1, t.y
    if t.x < t.level.width and not t.level:getTileCollisionAt(nextX, nextY) and not t.level:getDoorCollisionAt(nextX, nextY) then
      local canMove = true
      local obj = t.level:getObjectAt(nextX, nextY)
      if obj ~= nil then
        canMove = t.level:pushObjectRight(nextX, nextY, t)
      end

      if canMove == true then
        local dudeInWay, msg = t.level:getDudeAt(nextX, nextY)
        if dudeInWay == nil then
          return nil, msg
        end
        if dudeInWay then
          if force and not dudeInWay.isMoving then
            return false
          else
            t.level:onObjectStartedMoving(t, nextX, nextY)
            t.isMoving = true
            t.moveDir = moveDir
            t.dir = t.moveDir
            t.movePercent = 1
            dudeInWay:moveLeft(true)
            t.x = nextX
            return true
          end
        else
          t.level:onObjectStartedMoving(t, nextX, nextY)
          t.isMoving = true
          t.moveDir = moveDir
          t.dir = t.moveDir
          t.movePercent = 1
          t.x = nextX
          return true
        end
      end
    end
  end
  return false
end

-- Utility Methods --
function public.isValid(t)
  return (type(t) == "table" and t.__type == meta.__type)
end

function public.new(x, y, images, _level, colour, tilewidth, tileheight, speed)
  if images == nil or type(images) ~= "table" then
    return nil, "[Character] Cannot create character with images of type: "..type(images)
  end

  local spriteSheets = {
    up={},
    down={}
  }

  local msg = nil
  spriteSheets.up.left, msg = tilesheet.new(images["upleft"], tilewidth, tileheight)
  if spriteSheets.up.left == nil then
    return nil, msg
  end

  spriteSheets.up.right, msg = tilesheet.new(images["upright"], tilewidth, tileheight)
  if spriteSheets.up.right == nil then
    return nil, msg
  end

  spriteSheets.down.left, msg = tilesheet.new(images["downleft"], tilewidth, tileheight)
  if spriteSheets.down.left == nil then
    return nil, msg
  end

  spriteSheets.down.right, msg = tilesheet.new(images["downright"], tilewidth, tileheight)
  if spriteSheets.down.right == nil then
    return nil, msg
  end

  if not level.isValid(_level) then
    return nil, "[Character] Failed to create character with invalid level of type: "..type(_level)
  end

  if colour == nil then
    return nil, "[Character] Cannot create character with colour of type nil"
  end
  if colour < 1 or colour > 256 then
    return nil, "[Character] Character colour must be between 1 and 256"
  end

  local speed = speed or 4
  return setmetatable(
    {
      x=x,
      y=y,
      level=_level,
      colour=colour,
      speed=speed,
      dir=1,
      isMoving=false,
      offsetX=0,
      offsetY=0,
      moveDir=1,
      movePercent=0,
      spriteSheets=spriteSheets,
      currentSheet=spriteSheets.down.right,
      animFrame=1,
      animVertDir=2,
      animHoriDir=4
    }, meta
  )
end

return public
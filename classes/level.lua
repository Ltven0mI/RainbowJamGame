local public = {}
local private = {}
local meta = {}

  
-- TILEGRID
-- R == ID | G == SHEETID | B == STATE | A == ISSOLID
-- OBJECTGRID
-- R == ID | G == SHEETID | B == COLOUR | A == STATE [200 - 255]
-- TRIGGERGRID
-- R == ID | G == SHEETID | B == COLOUR | A == NETWORK
-- DOORGRID
-- R == ID | G == SHEETID | B == STATE | A == NETWORK
-- DUDEGRID
-- R == ID & COLOUR


-- Meta Data --
meta.__type = "level"

-- Meta Methods --
function meta.__index(t, key)
  return meta[key]
end


function meta.nextLevel()
  print("Next Level!")
end

function meta.preLevel()
  print("Pre Level!")
end

function meta.onObjectFinishedMoving(t, obj)
  if obj.currentTrigger ~= nil then
    if obj.currentTrigger.object == obj then
      meta.setTriggeredAt(t, obj.currentTrigger.x, obj.currentTrigger.y, false)
    end
    obj.currentTrigger = nil
  end

  if obj.nextTrigger ~= nil then
    obj.currentTrigger = obj.nextTrigger
    obj.nextTrigger = nil
  end
end

function meta.onObjectStartedMoving(t, obj, nextX, nextY)
  local nextTrigger = meta.getTrigger(t, nextX, nextY)
  if nextTrigger ~= nil then
    nextTrigger.object = obj
    meta.setTriggeredAt(t, nextX, nextY, true)
    obj.nextTrigger = nextTrigger
  end
end

function meta.getTrigger(t, x, y)
  if x <= 0 or y <= 0 or x > t.width or y > t.height then
    return nil, "[Level] GetTrigger out of bounds! x: "..x..", y: "..y
  end
  return t.triggerLayer.grid[y][x]
end

function meta.getExit(t, x, y)
  if x <= 0 or y <= 0 or x > t.width or y > t.height then
    return nil, "[Level] GetExit out of bounds! x: "..x..", y: "..y
  end
  return t.exitLayer.grid[y][x]
end

function meta.setTriggeredAt(t, x, y, isTriggered)
  if x <= 0 or y <= 0 or x > t.width or y > t.height then
    return nil, "[Level] SetTriggeredAt out of bounds! x: "..x..", y: "..y
  end
  local trigger = t.triggerLayer.grid[y][x]
  if trigger ~= nil then
    if trigger.colour == 256 or trigger.object.colour == trigger.colour then
      if t.buttonClickSound ~= nil and (trigger.triggered ~= isTriggered or trigger.isTriggered ~= isTriggered) then
        t.buttonClickSound:setPitch(0.9375 + math.random() / 8)
        t.buttonClickSound:play()
      end
      trigger.isTriggered = isTriggered
    else
      if t.buttonFailedSound ~= nil and (trigger.triggered ~= isTriggered or trigger.isTriggered ~= false) then
        t.buttonFailedSound:setPitch(0.9375 + math.random() / 8)
        t.buttonFailedSound:play()
      end
      trigger.isTriggered = false
    end
    trigger.triggered = isTriggered
  else
    return nil, "[Level] No Trigger at x: "..x..", y: "..y
  end
end

function meta.getNetworkState(t, networkID)
  if networkID < 256 then
    local network = t.triggerLayer.networks[networkID]
    if network ~= nil then
      for k, trigger in pairs(network) do
        if trigger.isTriggered == false then
          return false
        end
      end
      return true
    else
      return false
    end
  else
    local networkCount = 0
    for k, network in pairs(t.triggerLayer.networks) do
      for j, trigger in pairs(network) do
        if trigger.isTriggered == false then
          return false
        end
      end
      networkCount = networkCount + 1
    end
    if networkCount > 0 then
      return true
    end
  end
  return false
end

function meta.pushObjectUp(t, x, y, character)
  if x <= 0 or y <= 0 or x > t.width or y > t.height then
    return nil, "[Level] PushObjectUp out of bounds! x: "..x..", y: "..y
  end
  local obj = meta.getObjectAt(t, x, y)
  if obj == nil then
    return nil, "[Level] Failed to push object! No object at x: "..x..", y: "..y
  end

  if character.colour ~= obj.colour and obj.colour ~= 256 then return false end

  if not obj.isMoving then
    local nextX, nextY = obj.x, obj.y - 1
    if obj.y > 1 and not meta.getTileCollisionAt(t, nextX, nextY) and not meta.getDoorCollisionAt(t, nextX, nextY) then
      local nextObj = meta.getObjectAt(t, nextX, nextY)

      if nextObj == nil then
        local dudeInWay, msg = meta.getDudeAt(t, nextX, nextY)
        if dudeInWay == nil then
          return nil, msg
        end
        if dudeInWay ~= false then
          if not dudeInWay:moveUp(true) then
            return false  -- REMOVE ONCE DUDE POOFS
          end
        end
        meta.onObjectStartedMoving(t, obj, nextX, nextY)

        obj.y = obj.y - 1
        t.objectLayer.grid[y][x] = nil
        t.objectLayer.grid[obj.y][obj.x] = obj

        obj.isMoving = true
        obj.moveDir = 1
        obj.movePercent = 1
        return true
      end
    end
  end
  return false
end

function meta.pushObjectDown(t, x, y, character)
  if x <= 0 or y <= 0 or x > t.width or y > t.height then
    return nil, "[Level] PushObjectDown out of bounds! x: "..x..", y: "..y
  end
  local obj = meta.getObjectAt(t, x, y)
  if obj == nil then
    return nil, "[Level] Failed to push object! No object at x: "..x..", y: "..y
  end

  if character.colour ~= obj.colour and obj.colour ~= 256 then return false end

  if not obj.isMoving then
    local nextX, nextY = obj.x, obj.y + 1
    if obj.y < t.height and not meta.getTileCollisionAt(t, nextX, nextY) and not meta.getDoorCollisionAt(t, nextX, nextY) then
      local nextObj = meta.getObjectAt(t, nextX, nextY)

      if nextObj == nil then
        local dudeInWay, msg = meta.getDudeAt(t, nextX, nextY)
        if dudeInWay == nil then
          return nil, msg
        end
        if dudeInWay ~= false then
          if not dudeInWay:moveDown(true) then
            return false  -- REMOVE ONCE DUDE POOFS
          end
        end
        meta.onObjectStartedMoving(t, obj, nextX, nextY)

        obj.y = obj.y + 1
        t.objectLayer.grid[y][x] = nil
        t.objectLayer.grid[obj.y][obj.x] = obj

        obj.isMoving = true
        obj.moveDir = 2
        obj.movePercent = 1
        return true
      end
    end
  end
  return false
end

function meta.pushObjectLeft(t, x, y, character)
  if x <= 0 or y <= 0 or x > t.width or y > t.height then
    return nil, "[Level] PushObjectLeft out of bounds! x: "..x..", y: "..y
  end
  local obj = meta.getObjectAt(t, x, y)
  if obj == nil then
    return nil, "[Level] Failed to push object! No object at x: "..x..", y: "..y
  end

  if character.colour ~= obj.colour and obj.colour ~= 256 then return false end

  if not obj.isMoving then
    local nextX, nextY = obj.x - 1, obj.y
    if obj.x > 1 and not meta.getTileCollisionAt(t, nextX, nextY) and not meta.getDoorCollisionAt(t, nextX, nextY) then
      local nextObj = meta.getObjectAt(t, nextX, nextY)

      if nextObj == nil then
        local dudeInWay, msg = meta.getDudeAt(t, nextX, nextY)
        if dudeInWay == nil then
          return nil, msg
        end
        if dudeInWay ~= false then
          if not dudeInWay:moveLeft(true) then
            return false  -- REMOVE ONCE DUDE POOFS
          end
        end
        meta.onObjectStartedMoving(t, obj, nextX, nextY)

        obj.x = obj.x - 1
        t.objectLayer.grid[y][x] = nil
        t.objectLayer.grid[obj.y][obj.x] = obj

        obj.isMoving = true
        obj.moveDir = 3
        obj.movePercent = 1
        return true
      end
    end
  end
  return false
end

function meta.pushObjectRight(t, x, y, character)
  if x <= 0 or y <= 0 or x > t.width or y > t.height then
    return nil, "[Level] PushObjectRight out of bounds! x: "..x..", y: "..y
  end
  local obj = meta.getObjectAt(t, x, y)
  if obj == nil then
    return nil, "[Level] Failed to push object! No object at x: "..x..", y: "..y
  end

  if character.colour ~= obj.colour and obj.colour ~= 256 then return false end

  if not obj.isMoving then
    local nextX, nextY = obj.x + 1, obj.y
    if obj.x > 1 and not meta.getTileCollisionAt(t, nextX, nextY) and not meta.getDoorCollisionAt(t, nextX, nextY) then
      local nextObj = meta.getObjectAt(t, nextX, nextY)

      if nextObj == nil then
        local dudeInWay, msg = meta.getDudeAt(t, nextX, nextY)
        if dudeInWay == nil then
          return nil, msg
        end
        if dudeInWay ~= false then
          if not dudeInWay:moveRight(true) then
            return false  -- REMOVE ONCE DUDE POOFS
          end
        end
        meta.onObjectStartedMoving(t, obj, nextX, nextY)

        obj.x = obj.x + 1
        t.objectLayer.grid[y][x] = nil
        t.objectLayer.grid[obj.y][obj.x] = obj

        obj.isMoving = true
        obj.moveDir = 4
        obj.movePercent = 1
        return true
      end
    end
  end
  return false
end


function meta.update(t, dt)
  local dudeLayer = t.dudeLayer
  for k, dude in ipairs(dudeLayer.list) do
    dude:update(dt)
  end

  for k, door in ipairs(t.doorLayer.list) do
    local networkState = meta.getNetworkState(t, door.networkID)
    if networkState == false and door.open == true and meta.getObjectAt(t, door.x, door.y) == nil and meta.getDudeAt(t, door.x, door.y) == false then
      door.open = false
    elseif networkState == true and door.open == false then
      door.open = true
    end
  end

  for k, obj in ipairs(t.objectLayer.list) do
    if obj.isMoving then
      obj.movePercent = obj.movePercent - obj.speed * dt
      if obj.movePercent <= 0 then
        obj.movePercent = 0
        obj.isMoving = false

        meta.onObjectFinishedMoving(t, obj)
      end

      if obj.moveDir == 1 then
        obj.offsetY = obj.movePercent
      elseif obj.moveDir == 2 then
        obj.offsetY = -obj.movePercent
      elseif obj.moveDir == 3 then
        obj.offsetX = obj.movePercent
      elseif obj.moveDir == 4 then
        obj.offsetX = -obj.movePercent
      end
    end
  end
end


local rot_0 = math.rad(0)
local rot_90 = math.rad(90)
local rot_180 = math.rad(180)
local rot_270 = math.rad(270)

function meta.draw(t, x, y, sx, sy)
  local x, y = x or 0, y or 0
  local sx, sy = sx or 1, sy or 1
  local tilewidth, tileheight = t.tilewidth * sx, t.tileheight * sy
  local halfW, halfH = math.floor(tilewidth / 2), math.floor(tileheight / 2)

  for i=1, math.min(#t.layers, 2) do
    local layer = t.layers[i]
    love.graphics.draw(layer.drawnGrid, x, y, 0, sx, sy)
  end

  local triggerLayer = t.triggerLayer
  for k, trigger in ipairs(triggerLayer.list) do
    local sheet = t.tilesheets[trigger.sheetID]
    local tile, msg = sheet:getTile(trigger.id)
    if trigger.triggered then
      tile, msg = sheet:getTile(trigger.id + 1)
    end
    local drawX, drawY = x + (trigger.x-1) * tilewidth, y + (trigger.y-1) * tileheight
    love.graphics.draw(sheet.image, tile, drawX, drawY, 0, sx, sy)
  end

  local dudeLayer = t.dudeLayer
  for k, dude in ipairs(dudeLayer.list) do
    if k ~= dudeLayer.activeID then
      dude:draw(x, y, sx, sy)
    end
  end
  dudeLayer.list[dudeLayer.activeID]:draw(x, y, sx, sy)

  local objLayer = t.objectLayer
  for k, obj in ipairs(objLayer.list) do
    local state = obj.state
    local sheet = t.tilesheets[obj.sheetID]
    local tile, msg = sheet:getTile(obj.id)

    local rotation = 0
    if state == 1 then rotation = rot_0 end
    if state == 2 then rotation = rot_90 end
    if state == 3 then rotation = rot_180 end
    if state == 4 then rotation = rot_270 end
    local drawX, drawY = x + math.floor((0.5 + obj.offsetX + obj.x-1) * tilewidth), y + math.floor((0.5 + obj.offsetY + obj.y-1) * tileheight)
    love.graphics.draw(sheet.image, tile, drawX, drawY, rotation, sx, sy, halfW, halfH)
  end

  for i=3, #t.layers do
    local layer = t.layers[i]
    love.graphics.draw(layer.drawnGrid, x, y, 0, sx, sy)
  end

  local doorLayer = t.doorLayer
  for k, door in ipairs(doorLayer.list) do
    local state = door.state
    local sheet = t.tilesheets[door.sheetID]
    local tile, msg = sheet:getTile(door.id)
    if not door.open then
      local rotation = 0
      if state == 1 then rotation = rot_0 end
      if state == 2 then rotation = rot_90 end
      if state == 3 then rotation = rot_180 end
      if state == 4 then rotation = rot_270 end
      local drawX, drawY = x + math.floor((0.5 + door.x-1) * tilewidth), y + math.floor((0.5 + door.y-1) * tileheight)
      love.graphics.draw(sheet.image, tile, drawX, drawY, rotation, sx, sy, halfW, halfH)
    end
  end
end

function meta.drawToCanvas(t)
  local result = love.graphics.newCanvas(t.width * t.tilewidth, t.height * t.tileheight)
  result:renderTo(function() meta.draw(t) end)
  return result
end

function meta.takeScreenshot(t, filename)
  local filename = filename or os.date("screenshot-%Y-%m-%d-%H-%M-%S.png")
  local canvas = meta.drawToCanvas(t)
  local imageData = canvas:newImageData()
  local fileData = imageData:encode("png", filename)

  -- local fileBytes = fileData:getString()
  -- local file = io.open(path, "wb")
  -- file:write(fileBytes)
  -- io.close(file)

  -- love.filesystem.remove("screenshot")
end

function meta.getTileCollisionAt(t, x, y)
  if x <= 0 or y <= 0 or x > t.width or y > t.height then
    return nil, "[Level] GetTileCollisionAt out of bounds! x: "..x..", y: "..y
  end
  local solid = t.layers[1].solidGrid[y][x]
  return (solid > 200 or solid == 1)
end

function meta.getDoorCollisionAt(t, x, y)
  local door = t.doorLayer.grid[y][x]
  if door == nil then return false end
  return not door.open
end

function meta.getCollisionAt(t, x, y)
  if x <= 0 or y <= 0 or x > t.width or y > t.height then
    return nil, "[Level] GetCollisionAt out of bounds! x: "..x..", y: "..y
  end
  if meta.getTileCollisionAt(t, x, y) == true then return true end
  if meta.getObjectAt(t, x, y) ~= nil then return true end
  if meta.getDoorCollisionAt(t, x, y) == true then return true end
  if meta.getDudeAt(t, x, y) ~= nil then return true end
  return false
end

function meta.getObjectAt(t, x, y)
  if x <= 0 or y <= 0 or x > t.width or y > t.height then
    return nil, "[Level] GetObjectAt out of bounds! x: "..x..", y: "..y
  end
  return t.objectLayer.grid[y][x]
end

function meta.getDudeAt(t, x, y)
  if x <= 0 or y <= 0 or x > t.width or y > t.height then
    return nil, "[Level] GetDudeAt out of bounds! x: "..x..", y: "..y
  end

  for k, dude in ipairs(t.dudeLayer.list) do
    if dude.x == x and dude.y == y then
      return dude
    end
  end

  return false
end

-- Utility Methods --
function public.isValid(t)
  return (type(t) == "table" and t.__type == meta.__type)
end

function public.new(w, h)
  if w <= 0 then
    return nil, "[Level] Cannot create a level with a width of: "..w
  end
  if h <= 0 then
    return nil, "[Level] Cannot create a level with a height of: "..h
  end

  return setmetatable(
    {
      width=w,
      height=h
    }, meta
  )
end


local function createGrid(w, h, filler)
  local result = {}
  for y=1, h do
    result[y] = {}
    if filler ~= nil then
      for x=1, w do
        result[y][x] = filler
      end
    end
  end
  return result
end

local function drawLayer(layer, tilewidth, tileheight, tilesheets)
  local canvas = love.graphics.newCanvas(layer.width * tilewidth, layer.height * tileheight)
  local halfW, halfH = math.floor(tilewidth / 2), math.floor(tileheight / 2)

  love.graphics.setColor(255, 255, 255, 255)
  canvas:renderTo(function()
    for yi=1, layer.height do
      for xi=1, layer.width do
        local state = layer.stateGrid[yi][xi]
        local isSolid = layer.solidGrid[yi][xi]
        if isSolid > 1 then
          local id, sheetID = layer.idGrid[yi][xi], layer.sheetGrid[yi][xi]
          local sheet = tilesheets[sheetID]
          local tile, msg = sheet:getTile(id)
          if tile == nil then
            error("[Level] Failed to get tile to draw! "..msg)
          end
          local rotation = 0
          if state == 1 then rotation = rot_0 end
          if state == 2 then rotation = rot_90 end
          if state == 3 then rotation = rot_180 end
          if state == 4 then rotation = rot_270 end
          local drawX, drawY = halfW + (xi-1) * tilewidth, halfH + (yi-1) * tileheight
          love.graphics.draw(sheet.image, tile, drawX, drawY, rotation, 1, 1, halfW, halfH)
          -- love.graphics.print(""..id, drawX-halfW, drawY-halfH)
        end
      end
    end
  end)

  return canvas
end

function private.extractLayer(image, tilewidth, tileheight, tilesheets)
  local w, h = image:getDimensions()

  local result = {
    width = w,
    height = h,
    idGrid = createGrid(w, h),
    sheetGrid = createGrid(w, h),
    stateGrid = createGrid(w, h),
    solidGrid = createGrid(w, h)
  }

  local imageData = image:getData()
  for yi=1, h do
    for xi=1, w do
      local r, g, b, a = imageData:getPixel(xi-1, yi-1)
      result.idGrid[yi][xi] = r + 1
      result.sheetGrid[yi][xi] = g + 1
      result.stateGrid[yi][xi] = b + 1
      result.solidGrid[yi][xi] = a + 1
    end
  end

  result.drawnGrid = drawLayer(result, tilewidth, tileheight, tilesheets)

  return result, w, h
end

function private.extractObjectLayer(image)
  local w, h = image:getDimensions()

  local result = {
    width = w,
    height = h,
    list = {},
    grid = createGrid(w, h)
  }

  local imageData = image:getData()
  for yi=1, h do
    for xi=1, w do
      local r, g, b, a = imageData:getPixel(xi-1, yi-1)
      if a > 199 then
        local obj = {
          x=xi,
          y=yi,
          id=r+1,
          sheetID=g+1,
          colour=b+1,
          state=a-199,
          speed=4,
          isMoving=false,
          offsetX=0,
          offsetY=0,
          moveDir=1,
          movePercent=0
        }
        table.insert(result.list, obj)
        result.grid[yi][xi] = obj
      end
    end
  end

  return result, w, h
end

function private.extractTriggerLayer(image)
  local w, h = image:getDimensions()

  local result = {
    width = w,
    height = h,
    list={},
    grid=createGrid(w, h),
    networks={}
  }

  local imageData = image:getData()
  for yi=1, h do
    for xi=1, w do
      local r, g, b, a = imageData:getPixel(xi-1, yi-1)
      if a > 0 then
        local trigger = {
          x=xi,
          y=yi,
          triggered=false,
          isTriggered=false,
          id=r+1,
          sheetID=g+1,
          colour=b+1,
          networkID=a+1
        }
        table.insert(result.list, trigger)
        result.grid[yi][xi] = trigger
        if result.networks[trigger.networkID] == nil then result.networks[trigger.networkID] = {} end
        table.insert(result.networks[trigger.networkID], trigger)
      end
    end
  end

  return result, w, h
end

function private.extractDoorLayer(image)
  local w, h = image:getDimensions()

  local result = {
    width = w,
    height = h,
    list={},
    grid=createGrid(w, h)
  }

  local imageData = image:getData()
  for yi=1, h do
    for xi=1, w do
      local r, g, b, a = imageData:getPixel(xi-1, yi-1)
      if a > 0 then
        local door = {
          x=xi,
          y=yi,
          open=false,
          id=r+1,
          sheetID=g+1,
          state=b+1,
          networkID=a+1
        }
        table.insert(result.list, door)
        result.grid[yi][xi] = door
      end
    end
  end

  return result, w, h
end

function private.extractDudeLayer(image, dudeImages, _level, tilewidth, tileheight)
  local w, h = image:getDimensions()

  local result = {
    width = w,
    height = h,
    activeID=1,
    list={},
    grid=createGrid(w, h)
  }

  local imageData = image:getData()
  for yi=1, h do
    for xi=1, w do
      local r, g, b, a = imageData:getPixel(xi-1, yi-1)
      if a > 0 then
        local dude = character.new(xi, yi, dudeImages[r+1], _level, r+1, tilewidth, tileheight)
        result.list[r+1] = dude
      end
    end
  end

  return result, w, h
end

function private.extractExitLayer(image)
  local w, h = image:getDimensions()

  local result = {
    width = w,
    height = h,
    grid=createGrid(w, h)
  }

  local imageData = image:getData()
  for yi=1, h do
    for xi=1, w do
      local r, g, b, a = imageData:getPixel(xi-1, yi-1)
      if a > 0 then
        result.grid[yi][xi] = r+1
      end
    end
  end

  return result, w, h
end

function public.loadFromImage(layerImages, objectImage, triggerImage, doorImage, dudeImage, exitImage, tilesheets, dudeImages, buttonClickSound, buttonFailedSound)
  local result = nil
  local layers = {}
  local objectLayer = nil
  local triggerLayer = nil
  local doorLayer = nil
  local dudeLayer = nil
  local exitLayer = nil
  local w, h = -1, -1
  local tilewidth, tileheight = -1, -1

  result = setmetatable({}, meta)

  local tilesheets = tilesheets
  if tilesheet.isValid(tilesheets) then
    tilesheets = {tilesheets}
  elseif type(tilesheets) ~= "table" then
    return nil, "[Level] Cannot create level with tilesheets of type: "..type(tilesheets)
  end

  tilewidth = tilesheets[1].tilewidth
  tileheight = tilesheets[1].tileheight

  if layerImages == nil then
    return nil, "[Level] Cannot create level with layerImages of type nil!"
  elseif type(layerImages) == "table" then
    for k, v in ipairs(layerImages) do
      local layer, layerW, layerH = private.extractLayer(v, tilewidth, tileheight, tilesheets)
      if w == -1 and h == -1 then
        w = layerW
        h = layerH
      elseif layerW ~= w or layerH ~= h then
        return nil, "[Level] Failed to load level from images! Inconsistant layer sizes!"
      end
      table.insert(layers, layer)
    end
  elseif layerImages:type() == "Image" then
    local layer, layerW, layerH = private.extractLayer(layerImages, tilewidth, tileheight, tilesheets)
    w = layerW
    h = layerH
    table.insert(layers, layer)
  else
    return nil, "[Level] Cannot create level with layerImages of type: "..type(layerImages)
  end

  if objectImage == nil then
    return nil, "[Level] Cannot create level with objectImage or type nil!"
  elseif type(objectImage) == "userdata" and objectImage:type() == "Image" then
    local msg
    objectLayer, msg = private.extractObjectLayer(objectImage)
    if objectLayer == nil then
      return nil, msg
    end
  end

  if triggerImage == nil then
    return nil, "[Level] Cannot create level with triggerImage or type nil!"
  elseif type(triggerImage) == "userdata" and triggerImage:type() == "Image" then
    local msg
    triggerLayer, msg = private.extractTriggerLayer(triggerImage)
    if triggerLayer == nil then
      return nil, msg
    end
  end

  if doorImage == nil then
    return nil, "[Level] Cannot create level with doorImage or type nil!"
  elseif type(doorImage) == "userdata" and doorImage:type() == "Image" then
    local msg
    doorLayer, msg = private.extractDoorLayer(doorImage)
    if doorLayer == nil then
      return nil, msg
    end
  end

  if exitImage == nil then
    return nil, "[Level] Cannot create level with exitImage or type nil!"
  elseif type(exitImage) == "userdata" and exitImage:type() == "Image" then
    local msg
    exitLayer, msg = private.extractExitLayer(exitImage)
    if exitLayer == nil then
      return nil, msg
    end
  end

  if dudeImages == nil then
      return nil, "[Level] Cannot create level with dudeImages of type nil!"
  elseif type(dudeImages) ~= "table" then
      return nil, "[Level] Cannot create level with dudeImages of type "..type(dudeImages)
  end

  if dudeImage == nil then
    return nil, "[Level] Cannot create level with dudeImage or type nil!"
  elseif type(dudeImage) == "userdata" and dudeImage:type() == "Image" then
    local msg
    dudeLayer, msg = private.extractDudeLayer(dudeImage, dudeImages, result, tilesheets[1].tilewidth, tilesheets[1].tileheight)
    if dudeLayer == nil then
      return nil, msg
    end
  end

  -- if (dudes ~= nil and type(dudes) ~= "table") or dudes == nil then
  --   return nil, "[Level] Cannot create level with dudes of type: "..type(dudes)
  -- end

  result.width = w
  result.height = h
  result.tilewidth = tilesheets[1].tilewidth
  result.tileheight = tilesheets[1].tileheight
  result.layers = layers
  result.objectLayer = objectLayer
  result.triggerLayer = triggerLayer
  result.doorLayer = doorLayer
  result.dudeLayer = dudeLayer
  result.exitLayer = exitLayer
  result.tilesheets = tilesheets
  result.buttonClickSound = buttonClickSound
  result.buttonFailedSound = buttonFailedSound

  return result
end

return public
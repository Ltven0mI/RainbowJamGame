local public = {}
local private = {}
local meta = {}

-- Meta Data --
meta.__type = "tilesheet"

-- Meta Methods --
function meta.__index(t, key)
  return meta[key]
end

function meta.getTile(t, index)
  if index <= 0 or index > t.tileCount then
    return nil, "[Tilesheet] GetTile out of bounds: "..index
  end
  return t.tiles[index]
end

-- Utility Methods --
function public.isValid(t)
  return (type(t) == "table" and t.__type == meta.__type)
end

function private.checkTileEmpty(image, quad)
  local x, y, w, h = quad:getViewport()
  local imageData = image:getData()

  for xi=x, x+w-1 do
    for yi=y, y+h-1 do
      local r, g, b, a = imageData:getPixel(xi, yi)
      if a ~= 0 then
        return false
      end
    end
  end
  return true
end

function private.getTiles(image, tilewidth, tileheight)
  local tiles = {}
  local tileCountX = math.ceil(image:getWidth() / tilewidth)
  local tileCountY = math.ceil(image:getHeight() / tileheight)
  if tileCountX * tileCountY > 256 then
    return nil, "[Tilesheet] Tried to create a tilesheet with width: "..tileCountX..", height: "..tileCountY..". Tilesheets cannot hold more than 256 tiles."
  end

  local imageW, imageH = image:getDimensions()
  for y=1, tileCountY do
    for x=1, tileCountX do
      local quad = love.graphics.newQuad((x-1) * tilewidth, (y-1) * tileheight, tilewidth, tileheight, imageW, imageH)
      if not private.checkTileEmpty(image, quad) then
        table.insert(tiles, quad)
      end
    end
  end

  return tiles
end

function public.new(image, tilewidth, tileheight)
  if image == nil or image:type() ~= "Image" then
    return nil, "[Tilesheet] Cannot create tilesheet with image of type: "..type(image)
  end

  if tilewidth <= 0 or tileheight <= 0 then
    return nil, "[Tilesheet] Failed to create tilesheet! tilewidth and tileheight must be greater than 0."
  end

  local tiles, msg = private.getTiles(image, tilewidth, tileheight)
  if tiles == nil then
    return nil, msg
  end

  return setmetatable(
    {
      tilewidth=tilewidth,
      tileheight=tileheight,
      tiles=tiles,
      tileCount=#tiles,
      image=image
    }, meta
  )
end

return public